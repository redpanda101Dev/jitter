/**
The Main Class
*/
public class main{
    //Class Variables
    private static OStream ostream = new OStream(); //Output Stream
    private static IStream input = new IStream(); //Input Stream
    //Methods
    public static String lineBreak(){
        String rturn = "";
        for (int i =0; i<50; i++){
            rturn += '-';
        }
        return rturn;
    }
    private static void Help(){
        OStream.println("Jitter : Project Management\n"+ lineBreak());
        OStream.println("new : Create A New Project");
        OStream.println(lineBreak());
    }
    private static void Project(int ALength, String[] args){
        //I there is 1 argument Start Project Wizard 
        if (ALength == 1){
            //Creating A New Project
            Project_Wizard pwizard = new Project_Wizard();
            try{
                pwizard.New_Project_Wizard(); //Try .. Catch So It Can Throw An Error
            }catch(Exception e){
                OStream.println("An Error Occurred");
            }
        }else if( ALength == 4){
            Project_Wizard pwizard = new Project_Wizard();
            try{
             pwizard.New_Project(args[1], args[2], args[3]);
            }
            catch (Exception e){
                OStream.println("Sorry, An Error Occurred");
            }
        }

        else if( ALength == 2){
            Project_Wizard pwizard = new Project_Wizard();
            try{
                pwizard.New_Project_With_Name(args[1]);
            }catch ( Exception e ){
                OStream.println("An Error Occurred");
            }
        }
    }
    /**
    The main method
    @param args the command line arguments
    */
    public static void main(String[] args){
        int ALength = args.length;
        if(ALength != 0){
           switch (args[0]){
            case "new":
                Project(ALength, args); //Creating A New Project
                break;
            case "build":
                Build builder = new Build();
                builder.build();
                break;
            default:
                Help();
                break;
           }
        }else{
            Help();
        }        
    }
}
