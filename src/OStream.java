/**
<h1>Outstream Class</h1>
*/

public class OStream{
    /**
    <b>Constructor</b>
    */
    public OStream ( ){
        
    }
    /**
    <b>Basic String Print Method</b>
    <p>Prints To The Console Without a newline, ONLY Strings</p>
    @param message The Message To Print
    */
    public static void print( String message ){
       System.out.print( message );
       
    }
    /**
    <b>Basic String Print-Line Method</b>
    <p>Prints A Line The Console <b>WITH</b> a newline</p>
    @param message The Massage To Print
    */
    public static void println( String message ){
        System.out.println( message );
    }
}
