/**
Input Stream Class
*/


//Imports
import java.util.Scanner;
public class IStream{
    private static Scanner sc = new Scanner(System.in);
    public static String next(){
        String tmp = sc.nextLine();
        return tmp;
    }
}
