import java.util.Formatter; //Imports File Stuff
import java.io.*;
public class Project_Wizard{
    public static OStream o = new OStream();
    public static IStream i = new IStream();
    /**
        Method For A New Project Without Project Details
        
    */
       private static void printLines(String name, InputStream ins) throws Exception {
    String line = null;
    BufferedReader in = new BufferedReader(
        new InputStreamReader(ins));
    while ((line = in.readLine()) != null) {
        System.out.println(name + " " + line);
    }
  }

  private static void runProcess(String command) throws Exception {
    Process pro = Runtime.getRuntime().exec(command);
    printLines(command + " stdout:", pro.getInputStream());
    printLines(command + " stderr:", pro.getErrorStream());
    pro.waitFor();
    System.out.println(command + " exitValue() " + pro.exitValue());
  }
    public static void New_Project_Wizard() throws FileNotFoundException{
        o.println("Jitter : Java Project Managment\n"+ main.lineBreak());
        o.print("Name Of Project: ");
        String name = i.next();
        o.print("Description of Project: ");
        String description = i.next();
        o.print("Git [Y/n]: ");
        String git = i.next();
        String[] about = {name, description, git};
        new File(name).mkdirs();
        new File( name +"/src").mkdirs();
        File main = new File(name+"/src/main.java");
        File About = new File(name+"/about.xml");
        try
        {   
            PrintWriter pWrite = new PrintWriter(main);
            pWrite.println ("public class main{\npublic static void main(String[] args){\n//Code\n}\n}");
            pWrite.close();
            PrintWriter aWrite = new PrintWriter(About);
            String About_Content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            + "<!-- Made With Love, Care & Jitter -->\n"
            + "<about>\n" //about.xml 
            + "    <name>" + name + "</name>\n" 
            + "    <description>" + description + "</description>\n"
            + "    <git>"+ git + "</git>\n"
            + "    <build>\n"
            + "        <files>src/*</files>\n"
            + "        <output>build/</output>\n"
            + "        <jar>\n"
            + "            <manifest>Manifest.mf</manifest>\n"
            + "            <classes>build/*.class</classes>\n"
            + "            <out>build/Out.jar</out>\n"
            + "        </jar>\n"
            + "    </build>\n"
            + "</about>";
            o.print("XML Document: " + About_Content + "\n Is This Ok [Y/n]");
            String yn = i.next();
            if( yn != "n" && yn != "N"){
                aWrite.println(About_Content);
                aWrite.close();
            }else {
                System.exit(0);
            }
        }
    catch (FileNotFoundException ex)  
    {
        throw ex;
    }
}
    /**
    Method For A New Project With Only Name
    @param name Name For The Project
    */
    public static void New_Project_With_Name( String name ) throws FileNotFoundException{
        o.print("Description Of Project: ");
        String description = i.next();
        o.print("Git [Y/n]");
        String git = i.next();
        String[] about = {name, description, git};
        new File(name).mkdirs();
        new File( name +"/src").mkdirs();
        File main = new File(name+"/src/main.java");
        File About = new File(name+"/about.xml");
        try
        {   
            PrintWriter pWrite = new PrintWriter(main);
            pWrite.println ("public class main{\npublic static void main(String[] args){\n//Code\n}\n}");
            pWrite.close();
            PrintWriter aWrite = new PrintWriter(About);
            String About_Content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            + "<!-- Made With Love, Care & Jitter -->\n"
            + "<about>\n" //about.xml 
            + "    <name>" + name + "</name>\n" 
            + "    <description>" + description + "</description>\n"
            + "    <git>"+ git + "</git>\n"
            + "    <build>\n"
            + "        <files>src/*</files>\n"
            + "        <output>build/</output>\n"
            + "        <jar>\n"
            + "            <manifest>Manifest.mf</manifest>\n"
            + "            <classes>build/*.class</classes>\n"
            + "            <out>build/Out.jar</out>\n"
            + "        </jar>\n"
            + "    </build>\n"
            + "</about>";
            o.print("XML Document: " + About_Content + "\n Is This Ok [Y/n]");
            String yn = i.next();
                aWrite.println(About_Content);
                aWrite.close();
            
                System.exit(0);
            
            }
    catch (FileNotFoundException ex)  
    {
        throw ex;
    }
    }
    /** 
    Method For A New Project With All Details
    @param name The Name Of The Project
    @param description Description of the project
    @param Git Whether Git Should Be Used
    */
    public static void New_Project( String name, String description, String Git) throws FileNotFoundException{
        String[] arr = {name, description, Git}; //Pointless Array
        new File(name).mkdirs(); //Make Directories
        new File( name +"/src").mkdirs(); 
        File main = new File(name+"/src/main.java"); //Make Main.java
        File About = new File(name+"/about.xml"); //Make the build file
        try
        {   
            PrintWriter pWrite = new PrintWriter(main);
            pWrite.println ("public class main{\npublic static void main(String[] args){\n//Code\n}\n}");
            pWrite.close();
            PrintWriter aWrite = new PrintWriter(About);
            String About_Content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            + "<!-- Made With Love, Care & Jitter -->\n"
            + "<about>\n" //about.xml 
            + "    <name>" + name + "</name>\n" 
            + "    <description>" + description + "</description>\n"
            + "    <git>"+ Git + "</git>\n"
            + "    <build>\n"
            + "        <files>src/*</files>\n"
            + "        <output>build/</output>\n"
            + "        <jar>\n"
            + "            <manifest>Manifest.mf</manifest>\n"
            + "            <classes>build/*.class</classes>\n"
            + "            <out>build/Out.jar</out>\n"
            + "        </jar>\n"
            + "    </build>\n"
            + "</about>";
            
            
                aWrite.println(About_Content);
                aWrite.close();
            
                System.exit(0);
            
        }
    catch (FileNotFoundException ex)  
    {
        throw ex;
    }
    }
}
