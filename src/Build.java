import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import javax.xml.xpath.*;
public class Build{
     private static void printLines(String name, InputStream ins) throws Exception {
    String line = null;
    BufferedReader in = new BufferedReader(
        new InputStreamReader(ins));
    while ((line = in.readLine()) != null) {
        System.out.println(name + " " + line);
    }
  }

  private static void runProcess(String command) throws Exception {
    Process pro = Runtime.getRuntime().exec(command);
    printLines(command + " stdout:", pro.getInputStream());
    printLines(command + " stderr:", pro.getErrorStream());
    pro.waitFor();
    System.out.println(command + " exitValue() " + pro.exitValue());
  }
    
    public static void build(){
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse( new File ( "about.xml") );
            OStream.println("Started building\n" + main.lineBreak());           
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile("/about/build/files");
        NodeList files = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
        String buildSource = "./"+ files.item(0).getTextContent()+ ".java";
        String command = "javac " + buildSource;
        try{
            runProcess(command);
            
        }catch(Exception e){
            OStream.println("Sorry An Error Occured \n" + main.lineBreak());
            e.printStackTrace();
        }
        }
        catch( Exception e){
            OStream.println("Sorry An Error Occurred \n"+ main.lineBreak());
            e.printStackTrace();
        }
    }
}
